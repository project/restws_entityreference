<?php
/**
 * @file
 * RESTful web services entity references module file.
 */

/**
 * Default maximum depth of recursion for entity reference loads.
 */
define('RESTWS_ENTITYREFERENCES_MAX_DEPTH_DEFAULT', 2);

/**
 * Maximum and minimum allowed recursion for entity reference loads.
 */
define('RESTWS_ENTITYREFERENCES_MAX_DEPTH_MAX', 3);
define('RESTWS_ENTITYREFERENCES_MAX_DEPTH_MIN', 0);

/**
 * Implements hook_restws_meta_controls_alter().
 */
function restws_entityreference_restws_meta_controls_alter(&$controls) {
  $controls['load-entity-refs'] = 'load-entity-refs';
  $controls['max-depth'] = 'max-depth';
}

/**
 * Implements hook_restws_response_alter().
 */
function restws_entityreference_restws_response_alter(&$response, $function, $format_name) {
  if ($function != 'viewResource' && $function != 'queryResource' || !isset($_GET['load-entity-refs'])) {
    return;
  }

  $format = restws_format($format_name);

  $max_depth = filter_input(INPUT_GET, 'max-depth', FILTER_VALIDATE_INT, array(
    'options' => array(
      'default' => RESTWS_ENTITYREFERENCES_MAX_DEPTH_DEFAULT,
      'min_range' => RESTWS_ENTITYREFERENCES_MAX_DEPTH_MIN,
      'max_range' => RESTWS_ENTITYREFERENCES_MAX_DEPTH_MAX,
    ),
  ));

  $load_types = restws_entityreference_get_load_types(filter_input(INPUT_GET, 'load-entity-refs', FILTER_SANITIZE_STRING));

  if ($function == 'viewResource') {
    restws_entityreference_parse_entity_values($response, $load_types, $format, $max_depth);
  }
  elseif ($function == 'queryResource' && array_key_exists('list', $response)) {
    foreach ($response['list'] as &$entity) {
      restws_entityreference_parse_entity_values($entity, $load_types, $format, $max_depth);
    }
  }
}

/**
 * Check for and load entity reference fields on an entity.
 *
 * Parse each property of an entity, represented as a restws data response array
 * and load any fields that are identified as entity references.
 *
 * This is a recursive function that will traverse all discovered entity
 * references up to max_depth.
 *
 * @param array $entity
 *   The entity to parse for entity references.
 * @param array $load_types
 *   The entity types that should be expanded. Discovered entity reference types
 *   not in this array will be ignored.
 * @param RestWSFormatInterface $format
 *   The data response format.
 * @param int $max_depth
 *   The maximum depth to which entity references should be traversed. This
 *   value is decreased by one with every recursive call.
 */
function restws_entityreference_parse_entity_values(&$entity, array $load_types, RestWSFormatInterface $format, $max_depth) {
  if ($max_depth < 1) {
    return;
  }

  foreach ($entity as $property => &$value) {
    if (!is_array($value)) {
      continue;
    }

    if (array_key_exists('resource', $value) || array_key_exists('file', $value)) {
      restws_entityreference_parse_reference($value, $load_types, $format);
      restws_entityreference_parse_entity_values($value, $load_types, $format, $max_depth - 1);
    }

    if (!isset($value[0]) || !is_array($value[0]) || !array_key_exists('resource', $value[0]) && !array_key_exists('file', $value[0])) {
      continue;
    }

    foreach ($value as &$multivalue) {
      restws_entityreference_parse_reference($multivalue, $load_types, $format);
      restws_entityreference_parse_entity_values($multivalue, $load_types, $format, $max_depth - 1);
    }
  }
}

/**
 * Parse the data for a single entity reference.
 *
 * @param array $reference
 *   The data for a single referenced entity.
 * @param array $load_types
 *   The entity types that should be expanded. Discovered entity reference types
 *   not in this array will be ignored.
 * @param RestWSFormatInterface $format
 *   The data response format.
 */
function restws_entityreference_parse_reference(&$reference, array $load_types, RestWSFormatInterface $format) {
  if (array_key_exists('file', $reference)) {
    $reference = $reference['file'];
  }

  if (in_array($reference['resource'], $load_types)) {
    $controller = restws_entityreference_get_resource_controller($reference['resource']);
    $reference = restws_entityreference_load_entity_data($reference['resource'], $reference['id'], $format, $controller);
  }
}

/**
 * Load an entity via the RestWSEntityResourceController interface.
 *
 * Given an entity type and entity ID, load the data for that entity using the
 * RestWS entity controller and format classes.
 *
 * The RestWSEntityFormatInterface provides a public method for retrieving an
 * associative array representation of an entity, which is already used when
 * building the initial response. We simply call it directly, ensuring that all
 * data is fully consistent with the format of other restws responses. This also
 * ensures that access checking is properly handled for all fields.
 *
 * @param string $entity_type
 *   The entity type to load.
 * @param int $id
 *   The entity ID of the multivalue to load.
 * @param RestWSFormatInterface $format
 *   The format object with which the data is retrieved.
 * @param RestWSResourceControllerInterface $controller
 *   The resource controller object for the given entity type.
 *
 * @return array
 *   The array of data for the entity with the given ID, as loaded by the given
 *   format.
 */
function restws_entityreference_load_entity_data($entity_type, $id, RestWSFormatInterface $format, RestWSResourceControllerInterface $controller) {
  $wrapper = $controller->wrapper($id);
  $data = $format->getData($wrapper);

  drupal_alter('entity_data', $data, $controller, $wrapper, $format);

  return $data;
}

/**
 * Parse input limiting which entity types to expand.
 *
 * @param string $input
 *   The comma-delimited string of entity types that should be loaded.
 *
 * @return array
 *   The input entity types as an array.
 */
function restws_entityreference_get_load_types($input) {
  $entity_types = &drupal_static(__FUNCTION__);

  if (!isset($entity_types)) {
    $entity_types = array_keys(entity_get_info());
  }

  if (empty($input)) {
    return $entity_types;
  }

  return array_filter(explode(',', $input), function ($element) use ($entity_types) {
    return in_array($element, $entity_types);
  });
}

/**
 * Get the resource controller for the given entity type.
 *
 * @param string $entity_type
 *   The name of the entity type for which to retrieve a resource controller.
 *
 * @return RestWSResourceControllerInterface
 *   The resource controller instance.
 */
function restws_entityreference_get_resource_controller($entity_type) {
  $controllers = &drupal_static(__FUNCTION__);

  if (!isset($controllers)) {
    $controllers = array();
  }

  if (!isset($controllers[$entity_type])) {
    $controllers[$entity_type] = new RestWSEntityResourceController($entity_type, '');
  }

  return $controllers[$entity_type];
}
